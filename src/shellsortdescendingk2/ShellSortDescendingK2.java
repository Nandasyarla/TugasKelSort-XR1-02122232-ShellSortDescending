
package shellsortdescendingk2;

import java.util.Scanner;

public class ShellSortDescendingK2 {

  
    public static void main(String[] args) {
        Scanner input = new Scanner (System.in);
      
        int [] data= {30, 62, 53, 42, 17, 97, 91, 38};
        System.out.println();
            
        System.out.println("Data belum urut: ");
        for(int i=0;i<data.length;i++){
            System.out.printf("%d ",data[i]);
        }
      
        
        int jarak=data.length;
        boolean end;
        while (jarak>1){
            jarak/=2;
            end=false;
            while (end==false){
                end=true;
                for(int j=0;j<data.length-jarak;j++){
                    if (data[j]<data[j+jarak]){
                        int tukar=data[j];
                        data[j]=data[j+jarak];
                        data[j+jarak]=tukar;
                        end=false;
                    }
                }
            }   
        }
        System.out.println("\nData setelah urut: ");
        for(int i=0;i<data.length;i++){
            System.out.printf("%d ",data[i]);
    }
}
}
